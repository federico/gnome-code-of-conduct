# Transparency report for December 2019 - GNOME Code of Conduct Committee

This is the first transparency report from GNOME's Code of Conduct Committee.  We intend to publish these reports every three months.

Since this is the first report after GNOME's new [Code of Conduct](https://conduct.gnome.org) was put in place by the Board, the report will include issues that the relatively new Committee dealt with so far, even if they didn't occur within the last three months.

## Overview of the reports:

* One report about unwelcome sexual attention during GUADEC 2018 and subsequent private contact. This incident was resolved with a ban on GNOME Telegram channels.

* One report about unwelcoming attitude towards newcomers during GUADEC 2019. This incident was resolved after the conference.

* One report about racist remarks posted to a mailing list and in private to another person. This incident was resolved with a ban through moderation.

A summary of each each incident follows.

## Summary of reported incidents

* After GUADEC 2018, an attendee was reported for unwanted sexual attention and physical contact that happened during the conference. Further unwanted online attention occurred after the conference.  The incident had a high impact and warranted action. This person has been banned from the Telegram channels for a year.

* At GUADEC 2019, an attendee was reported twice for displaying an unwelcoming attitude towards newcomers, in the form of belittling jokes.  The CoC committee asked the reported person to be more welcoming towards newcomers.

* A non-Foundation member posted racist remarks to the a GNOME mailing list, and later contacted a Foundation member in private with racist remarks.  The reported person has been put under moderation.

## Credits

The Code of Conduct Committee would like to thank:

* The Foundation Board for ratifying the new Code of Conduct.
* Sage Sharp from Otter Tech LLC for their work in the new CoC and procedures.
* Everyone who came forward and reported possible violations.
* The volunteers for GUADEC 2019 who took a CoC enforcement training class and were available during the conference.
* The GNOME community at large for being supportive of the Code of Conduct.
