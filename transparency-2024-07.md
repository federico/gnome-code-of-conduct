# Transparency report for January to July 2024 - GNOME Code of Conduct Committee

GNOME's Code of Conduct is our community’s shared standard of behavior for participants in GNOME. This is the Code of Conduct Committee's periodic summary report of its activities from January to July 2024.

The current members of the CoC Committee are:

* Federico Mena Quintero (chairperson)
* Rosanna Yuen
* Anisa Kuci
* Michael Downey (liaison to the Board)

All the members of the CoC committee have completed Code of Conduct Incident Response training provided by Otter Tech, and are professionally trained to handle incident reports in GNOME community events.

The committee has an email address that can be used to send reports: conduct@gnome.org as well as a website for report submission: https://conduct.gnome.org/ 

## Reports

Since January 2024, the committee has received a total of 6 reports, by 6 different people.  Of these, two incidents were determined to be actionable by the committee, and were further resolved during the reporting period:

* One incident required a notification of inappropriate behavior, with no further consequences.

* One incident resulted in the Code of Conduct Committee recommending a ban, which is now in effect. Pursuant to the Foundation Bylaws, the Committee also recommended to the Board of Directors the consequence of removal of the reported individual’s Foundation membership status. The latter consequence was considered and approved by the Board, but has not yet been implemented.

## Meetings of the CoC committee

The CoC committee has monthly meetings for general updates, and weekly ad-hoc meetings when they receive reports. There are also in-person meetings during GNOME events.

## Ways to contact the CoC committee

* https://conduct.gnome.org - contains the GNOME Code of Conduct and a reporting form.
* conduct@gnome.org - incident reports, questions, etc.
