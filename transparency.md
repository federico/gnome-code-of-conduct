# Transparency Reports from the Code of Conduct Committee

Per the [Code of Conduct Committe Procedures](/committee-procedures#transparency-reports):

Once every three months, the GNOME Code of Conduct committee will provide a public transparency report about the resolved and ongoing reports it has handled. The committee may decide to delay a transparency report if the timing of releasing a transparency report would jeopardize the privacy of the reporter, the reported person, or third-party witnesses.

The transparency reports will remove any information about the reporter and the reported person. If there is no way to anonymize the report without revealing the identity of the reporter or the reported person, the transparency report will simply note that a report was made. If no reports have been made in the specified time period, the transparency report will state that. 

 * [December 2019](/transparency-2019-12)
 * [September 2020](/transparency-2020-09)
 * [December 2022](/transparency-2022-12)
 * [January 2024](/transparency-2024-01)
 * [July 2024](/transparency-2024-07)
 
