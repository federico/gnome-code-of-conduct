# Transparency report for September 2020 - GNOME Code of Conduct Committee

GNOME's Code of Conduct is our standard of behavior for participants in GNOME.  This is the Code of Conduct Committee's periodic report of activities.

## Reports

Since December 2019, the committee got a single, anonymous report that unfortunately is not actionable by the committee.  We have taken no action on it.

## Changes to GNOME's Code of Conduct

Since December 2019, there have been two changes to the Code of Conduct, as approved by the Foundation Board:

* Separate out the [Safety versus Comfort](https://conduct.gnome.org/#Safety_versus_Comfort) section and expand its explanatory text.  We added an extra page outside the Code of Conduct called [Supporting Diversity](https://conduct.gnome.org/supporting-diversity/).

* Integrate in-person and online events into the Scope section of the Code of Conduct.  With this, the old Code of Conduct for Events got deprecated; now GNOME has a single CoC for everything.

GUADEC 2020 was the last conference to use the old Code of Conduct for Events; GNOME Onboard Africa Virtual and GNOME Asia are the first conferences to use the new unfied CoC.

## Changes in the Code of Conduct Committee

The Board appointed Molly De Blanc into the CoC Committe as a replacement for Christel Dahlskjaer, whose period at the committee ended.

The current members of the CoC Committee are:

* Federico Mena Quintero (chairperson and liaison to the Board)
* Felipe Borges
* Rosanna Yuen
* Molly de Blanc 

## Easier ways to contact the CoC committee

We now have easier ways to contact the committee:

 * https://conduct.gnome.org - contains the GNOME Code of Conduct
 * conduct@gnome.org - incident reports, questions, etc.

These go respectively to code-of-conduct-committee@gnome.org and https://wiki.gnome.org/Foundation/CodeOfConduct.

Thanks to the sysadmin team for their quick response on these.
