GNOME Code of Conduct
=====================

This repository contains the GNOME Code of Conduct in a few formats:

* Markdown, in the `.md` files.
* MoinMoin wiki format, in the `wiki` directory.

GNOME's Code of Conduct was developed in contract with [Otter
Tech](https://otter.technology/).  It is released under the [Creative
Commons Attribution 3.0 Unported
License](http://creativecommons.org/licenses/by/3.0/).

You can see the [GNOME Code of Conduct's web
page](https://conduct.gnome.org) and the [CoC Committee
page](https://wiki.gnome.org/CodeOfConductCommittee#preview).
